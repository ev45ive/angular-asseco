export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists?: Artist[];
  images: AlbumImage[];
  tracks?: PagingObject<Track>;
}
export interface Track extends Entity {
  preview_url:string
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
