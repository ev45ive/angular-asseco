export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  /**
   * Hex color
   */
  color: string;
  tracks?: Track[]; // Array<Track>
}

export interface Track extends Entity {}

export interface Entity {
  id: number;
  name: string;
}

// if('string' == typeof p.id){
//   p.id
// }

// switch(e.type ) {
//   case 'Playlist': {
//     e
//   }
//   break;
//   case 'Track':{
//     e
//   }
// }

// export class Playlist implements IPlaylist {
//   id: number;
//   name: string;

//   constructor(id: number, name: string) {
//     super(id,name)

//     this.id = id;
//     this.name = name;
//   }
// }
// };
// new Playlist(p.id, p.name);
