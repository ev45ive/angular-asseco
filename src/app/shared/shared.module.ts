import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HighlightDirective } from "./highlight.directive";
import { UnlessDirective } from "./unless.directive";
import { CardComponent } from "./card/card.component";
import { TabsExamplesComponent } from "./tabs-examples/tabs-examples.component";
import { TabsComponent } from "./tabs/tabs.component";
import { TabComponent } from "./tab/tab.component";
import { TabsNavComponent } from "./tabs-nav/tabs-nav.component";

@NgModule({
  declarations: [
    HighlightDirective,
    UnlessDirective,
    CardComponent,
    TabsExamplesComponent,
    TabsComponent,
    TabComponent,
    TabsNavComponent
  ],
  imports: [CommonModule],
  exports: [
    HighlightDirective,
    UnlessDirective,
    CardComponent,
    TabsExamplesComponent
  ]
})
export class SharedModule {}
