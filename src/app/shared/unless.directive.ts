import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  ComponentFactoryResolver
} from "@angular/core";
import { PlaylistsViewComponent } from "../playlists/views/playlists-view/playlists-view.component";

type C = {
  $implicit: string;
  sosy: string;
};

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  @Input("appUnless")
  set hide(hide) {
    if (hide) {
      this.vcr.clear();
    } else {
      // debugger;
      const f = this.cfr.resolveComponentFactory(PlaylistsViewComponent);
      f.inputs;
      const c = this.vcr.createComponent(f, 0);
      // c.instance.playlists.push({
      //   id: 345,
      //   name: "Extra!",
      //   color: "#ff00ff",
      //   favorite: false
      // });
    }
  }

  constructor(
    private tpl: TemplateRef<C>,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    console.log("unless", tpl, vcr);
  }
}

// const context: C = {
//   $implicit: "placki !",
//   sosy: "malinowy"
// };
// this.vcr.createEmbeddedView(this.tpl, context, 0);
