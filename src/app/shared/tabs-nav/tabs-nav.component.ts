import { Component, OnInit, EventEmitter } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs-nav',
  templateUrl: './tabs-nav.component.html',
  styleUrls: ['./tabs-nav.component.scss']
})
export class TabsNavComponent implements OnInit {

  tabs: TabComponent[] = [];
  active: TabComponent;

  tabChange = new EventEmitter<TabComponent>()

  toggle(tab){
    this.tabChange.emit(tab)
  }

  constructor() { }

  ngOnInit() {
  }

}
