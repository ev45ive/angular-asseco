import { Component, OnInit, Input, Inject, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.scss"]
})
export class TabComponent implements OnInit {
  @Input() title;
  open = false;

  tabChange = new EventEmitter<TabComponent>()

  toggle(){
    this.tabChange.emit(this)
  }

  // constructor(private parent: TabsComponent) {
  //   parent.tabs.push(this);

    // console.log(parent);
  // }

  ngOnInit() {}
}
