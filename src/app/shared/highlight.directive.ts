import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host:{
  //   '[style.color]':'color',
  //   '(mouseenter)':'activate($event.x,$event.y)'
  // }
})
export class HighlightDirective implements OnInit {
  @Input("appHighlight")
  color;

  @HostBinding("style.color")
  @HostBinding("style.border-color")
  get activeColor() {
    return this.active ? this.color : "";
  }

  constructor(private elem: ElementRef) {
    // console.log("hello appHighlight", this.elem);
  }

  @HostBinding('class.fa')
  @HostBinding('class.fa-open')
  // @HostBinding('class.active')
  active = false;

  @HostListener("mouseenter", ["$event.x"])
  activate(x: number) {
    this.active = true;
  }

  @HostListener("mouseleave")
  devactivate() {
    this.active = false;
  }

  ngOnInit() {
    // console.log(this.color);
    // this.elem.nativeElement.style.color = this.color;
  }

  ngDoCheck() {
    // this.elem.nativeElement.style.color = this.color;
  }
}


// console.log(HighlightDirective)