import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";
import { SecurityModule } from "./security/security.module";
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    /* App Features */
    PlaylistsModule,
    // MusicSearchModule,
    /* Shared */
    SharedModule,
    SecurityModule,
    /* Routes ordering matters! */
    AppRoutingModule
  ],
  providers: [],
  // entryComponents:[AppComponent]
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap(){
  //   this.app.bootstrap(AppComponent, 'app-root')
  // }
}
