import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicSearchService } from "../../services/music-search.service";
import {
  tap,
  takeUntil,
  catchError,
  multicast,
  refCount,
  share,
  shareReplay
} from "rxjs/operators";
import {
  Subscription,
  Subject,
  EMPTY,
  ConnectableObservable,
  ReplaySubject
} from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"]
})
export class MusicSearchViewComponent implements OnInit {
  albums: Album[];

  results$ = this.service.getAlbums().pipe(shareReplay(1));

  query$ = this.service.query$;

  message: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {
    // const query = this.route.snapshot.queryParamMap.get("query");

    this.route.queryParamMap.subscribe(queryParamMap => {

      const query = queryParamMap.get("query");
      if (query) {
        this.service.search(query);
      }
    });
  }

  search(query: string) {
    this.router.navigate([], {
      queryParams: {
        query: query
      },
      relativeTo:this.route,
      replaceUrl:true
    });

    this.service.search(query);
  }

  ngOnInit() {}
}

// tap(albums => {
//   this.albums = albums;
//   this.message = "";
// }),
// catchError(err => {
//   this.message = err.message;
//   return EMPTY;
// })
