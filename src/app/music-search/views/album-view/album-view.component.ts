import { Component, OnInit } from "@angular/core";
import { Album } from "src/app/models/Album";
import { ActivatedRoute } from "@angular/router";
import { MusicSearchService } from "../../services/music-search.service";
import { Track } from '../../../models/Album';

@Component({
  selector: "app-album-view",
  templateUrl: "./album-view.component.html",
  styleUrls: ["./album-view.component.scss"]
})
export class AlbumViewComponent implements OnInit {
  tracksVisible = false;

  showTracks() {
    this.tracksVisible = true;
  }

  currentTrack?:Track

  album: Album;

  constructor(
    private service: MusicSearchService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    
    const r = this.service.fetchAlbum(this.route.snapshot.params.id)
    
    r.subscribe( album => {
      this.album = album
    })
  }
}
