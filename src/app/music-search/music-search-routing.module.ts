import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchViewComponent } from "./views/music-search-view/music-search-view.component";
import { AlbumViewComponent } from './views/album-view/album-view.component';

const routes: Routes = [
  {
    path: "",
    component: MusicSearchViewComponent,
    children: []
  },
  {
    path:':id',
    component: AlbumViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule {}
