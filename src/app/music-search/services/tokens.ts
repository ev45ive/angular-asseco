import { InjectionToken } from "@angular/core";
export const SEARCH_API_URL = new InjectionToken(
  "SEARCH_API_URL token for music search"
);
