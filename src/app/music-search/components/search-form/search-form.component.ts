import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  Validator,
  AsyncValidator,
  AsyncValidatorFn,
  AbstractControl,
  ValidationErrors
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  map,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer, combineLatest } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent {
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const hasError =
      "string" !== typeof control.value || control.value.includes("batman");

    return hasError
      ? {
          censor: { badword: "batman" }
        }
      : null;
  };

  asyncCensor: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    //return this.http.get('validateApi',{}).pipe(map(resp=resp.errrs))

    return Observable.create((observer: Observer<ValidationErrors | null>) => {
      // on:Subscribe
      const handler = setTimeout(() => {
        const errors = this.censor(control);
        observer.next(errors);
        observer.complete();
      }, 1000);

      /* Teardown - on:Unsubscribe */
      return () => {
        clearTimeout(handler);
      };
    });
  };

  @Input()
  set query(q) {
    this.queryForm.get("query").setValue(q, {
      // onlySelf:true,
      emitEvent: false
    });
  }

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor
      ],
      [this.asyncCensor]
    ),
    type: new FormControl("album")
  });

  constructor() {
    (window as any).form = this.queryForm;

    const field = this.queryForm.get("query");

    const valid$ = field.statusChanges.pipe(filter(s => s == "VALID"));

    const value$ = field.valueChanges;

    // const search$ = combineLatest(valid$, value$).pipe(
    //   map( ([valid,value]) => value )
    // )

    const search$ = valid$.pipe(
      withLatestFrom(value$),
      map(([valid, value]) => value)
    );

    search$
      .pipe(
        debounceTime(400),
        filter(query => query.length >= 3),
        distinctUntilChanged()
      )
      .subscribe(this.queryChange)
      
      // .subscribe(query => {
      //   this.search(query);
      // });
  }

  ngOnInit() {}

  @Output() queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }
}
