import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchViewComponent } from "./views/music-search-view/music-search-view.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_API_URL } from "./services/tokens";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MusicSearchService } from "./services/music-search.service";
import { AlbumViewComponent } from './views/album-view/album-view.component';

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumViewComponent
  ],
  imports: [
    CommonModule,
    MusicSearchRoutingModule,
    // HttpClientModule,
    ReactiveFormsModule
  ],
  // exports: [MusicSearchViewComponent],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.api_url
    },
    MusicSearchService
    // {
    //   provide: MusicSearchService,
    //   useFactory(url) {
    //     return new MusicSearchService(url);
    //   },
    //   deps: [SEARCH_API_URL]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService
    //   // deps: [TEST_MOCK_OTHER_SEARCH_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService
    // },
    // MusicSearchService,
  ]
})
export class MusicSearchModule {}
