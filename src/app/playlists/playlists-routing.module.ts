import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./views/playlists-view/playlists-view.component";
import { SelectedPlaylistComponent } from "./views/selected-playlist/selected-playlist.component";
import { SelectedPlaylistFormComponent } from "./views/selected-playlist-form/selected-playlist-form.component";
import { SelectedPlaylistsResolverService } from './services/selected-playlists-resolver.service';

const routes: Routes = [
  {
    path: "playlists",
    children: [
      {
        path: "",
        component: PlaylistsViewComponent,
        children: [
          {
            path: "",
            component: SelectedPlaylistComponent
          }
        ]
      },
      {
        path: ":playlist_id",
        component: PlaylistsViewComponent,
        resolve:{
          // user: ActiveLoggedUserResolver
          // placki: PlackiResolver,
          playlist: SelectedPlaylistsResolverService,
        },
        children: [
          {
            path: "",
            component: SelectedPlaylistComponent
          },
          {
            path: "edit",
            component: SelectedPlaylistFormComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
