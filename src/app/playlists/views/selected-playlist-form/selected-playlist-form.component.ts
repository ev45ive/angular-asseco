import { Component, OnInit } from "@angular/core";
import { map, filter, switchMap, shareReplay } from "rxjs/operators";
import { PlaylistsService } from '../../services/playlists.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Playlist } from 'src/app/models/Playlist';

@Component({
  selector: "app-selected-playlist-form",
  templateUrl: "./selected-playlist-form.component.html",
  styleUrls: ["./selected-playlist-form.component.scss"]
})
export class SelectedPlaylistFormComponent implements OnInit {
  
  selected$ = this.route.data.pipe(
    //
    map(data => data.playlist as Playlist)
  );
  
  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // this.router.resetConfig()
  }

  save(draft){
    this.service.save(draft)
    this.router.navigate(['..'],{
      relativeTo:this.route
    })
  }

  cancel(){
    this.router.navigate(['..'],{
      relativeTo:this.route
    })
  }

  ngOnInit() {}
}
