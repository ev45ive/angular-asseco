import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedPlaylistFormComponent } from './selected-playlist-form.component';

describe('SelectedPlaylistFormComponent', () => {
  let component: SelectedPlaylistFormComponent;
  let fixture: ComponentFixture<SelectedPlaylistFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedPlaylistFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedPlaylistFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
