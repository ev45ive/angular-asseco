import { Component, OnInit } from "@angular/core";
import { map, switchMap, shareReplay, filter } from "rxjs/operators";
import { PlaylistsService } from "../../services/playlists.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Playlist } from "src/app/models/Playlist";
@Component({
  selector: "app-selected-playlist",
  templateUrl: "./selected-playlist.component.html",
  styleUrls: ["./selected-playlist.component.scss"]
})
export class SelectedPlaylistComponent implements OnInit {

  
  selected$ = this.route.data.pipe(
    //
    map(data => data.playlist as Playlist)
  );

  constructor(private route: ActivatedRoute, private router: Router) {}

  edit() {
    this.router.navigate(["./edit"], {
      relativeTo: this.route
    });
  }

  ngOnInit() {}
}
