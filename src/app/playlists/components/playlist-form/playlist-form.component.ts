import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class PlaylistFormComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  onSave(draft:Partial<Playlist>) {
    
    const playlist:Playlist = {
      ...this.playlist,
      ...draft
    }
    
    this.save.emit(playlist);
  }
  
  constructor() {}

  ngOnInit() {}
}

// type PlaylistDraf = Partial<Playlist>

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

// type PlaylistDraft = {
//   [k in 'name'|'color']: Playlist[k]
// }