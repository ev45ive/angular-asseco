import { TestBed } from '@angular/core/testing';

import { SelectedPlaylistsResolverService } from './selected-playlists-resolver.service';

describe('SelectedPlaylistsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SelectedPlaylistsResolverService = TestBed.get(SelectedPlaylistsResolverService);
    expect(service).toBeTruthy();
  });
});
