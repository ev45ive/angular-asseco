import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,
  Router
} from "@angular/router";
import { Playlist } from "src/app/models/Playlist";
import { Observable, of } from "rxjs";
import { map, filter, switchMap, shareReplay } from "rxjs/operators";
import { PlaylistsService } from "./playlists.service";

@Injectable({
  providedIn: "root"
})
export class SelectedPlaylistsResolverService implements Resolve<Playlist> {
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Playlist> {

    // return this.service.fetchPlaylist(parseInt(route.paramMap.get('playlist_id')))
    
    return of(route.paramMap).pipe(
      map(paramMap => paramMap.get("playlist_id")),
      // Is there and ID?
      filter(id => !!id),
      map(id => parseInt(id)),
      switchMap(id => this.service.fetchPlaylist(id)),
      shareReplay()
    )
  }

  constructor(private service: PlaylistsService) {
    // this.router.resetConfig()
  }
}
