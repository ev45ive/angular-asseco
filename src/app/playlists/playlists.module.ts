import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsViewComponent } from "./views/playlists-view/playlists-view.component";
import { ItemsListComponent } from "./components/items-list/items-list.component";
import { ListItemComponent } from "./components/list-item/list-item.component";
import { PlaylistDetailsComponent } from "./components/playlist-details/playlist-details.component";
import { PlaylistFormComponent } from "./components/playlist-form/playlist-form.component";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { SelectedPlaylistComponent } from './views/selected-playlist/selected-playlist.component';
import { SelectedPlaylistFormComponent } from './views/selected-playlist-form/selected-playlist-form.component';

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    SelectedPlaylistComponent,
    SelectedPlaylistFormComponent
  ],
  entryComponents:[PlaylistsViewComponent],
  imports: [
    CommonModule, 
    FormsModule, 
    SharedModule,
    PlaylistsRoutingModule
  ],
  exports: [PlaylistsViewComponent]
})
export class PlaylistsModule {}
