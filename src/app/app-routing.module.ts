import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full" // "prefix"
  },
  {
    path:'music',
    loadChildren: () => 
      import('./music-search/music-search.module')
              .then(m => m.MusicSearchModule )
  },
  {
    path: "**",
    redirectTo: "playlists",
    pathMatch: "full" // "prefix"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      // useHash: true,
      paramsInheritanceStrategy:'always'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
