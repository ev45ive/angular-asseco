import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

// https://github.com/manfredsteyer/angular-oauth2-oidc

export class AuthConfig {
  auth_url: string;
  client_id: string;
  response_type: "token" | "code";
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string = null;

  constructor(private config: AuthConfig) {
    const p = new HttpParams({
      fromString: window.location.hash
    });

    const access_token = p.get("#access_token");

    if (access_token) {
      this.token = access_token;
      sessionStorage.setItem("token", this.token);
      location.hash = "";
    } else {
      this.token = sessionStorage.getItem("token");
    }
  }

  authorize() {
    sessionStorage.removeItem('token')
    
    const { client_id, redirect_uri, response_type, auth_url } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id, // = client_id
        redirect_uri,
        response_type
      }
    });

    const url = `${auth_url}?${p.toString()}`;
    location.href = (url);
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
